$(document).ready(function () {

  $("#card-number").off().on("blur", function () {
    if ($(this).val().length < 16) {
      $(this).addClass("has-error");
    }
    else {
      $(this).removeClass("has-error");
    }
  });

  $("#cvc-number").off().on("blur click focus", function () {
    if ($(this).val().length < 3) {
      $(this).addClass("has-error");
    }
    else {
      $(this).removeClass("has-error");
    }
  });

  var date = new Date();
  var currentMonth =date.getMonth();
  if(currentMonth<9){
    currentMonth = "0"+(1+currentMonth);
  }
  $("#exp-month").val(currentMonth);

});